package br.com.senai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.com.senai.model.Endereco;
import br.com.senai.model.Professor;
import br.com.senai.model.Telefone;
import br.com.senai.model.repository.IProfessorDAO;

@Controller
public class ProfessorBC {
	@Autowired
	private IProfessorDAO dao;
	@Autowired
	private EnderecoBC enderecoBC;
	@Autowired
	private TelefoneBC telefoneBC;
	
	public void salvarProfessor(Professor professor, Endereco endereco, Telefone telefone) {
		Professor professorSalvo = dao.saveAndFlush(professor);
		
		endereco.setPessoa(professorSalvo);
		enderecoBC.salvarEndereco(endereco);
		
		telefone.setPessoa(professorSalvo);
		telefoneBC.salvarTelefone(telefone);
	}

	public void excluirProfessor(Professor professor) {
		enderecoBC.excluirEnderecoPorPessoa(professor);
		
		telefoneBC.excluirTelefonePorPessoa(professor);
		
		dao.delete(professor);
	}

	public List<Professor> listarProfessor() {
		return dao.findAll();
	}
}
