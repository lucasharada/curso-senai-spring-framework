package br.com.senai.controller;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.com.senai.model.Aluno;
import br.com.senai.model.Endereco;
import br.com.senai.model.Telefone;
import br.com.senai.model.repository.IAlunoDAO;

@Controller
public class AlunoBC {
	@Autowired
	private IAlunoDAO dao;
	@Autowired
	private EnderecoBC enderecoBC;
	@Autowired
	private TelefoneBC telefoneBC;

	public void salvarAluno(Aluno aluno, Endereco endereco, Telefone telefone) {
		if (aluno.getMatricula() == 0){
			aluno.setMatricula(Integer.parseInt(RandomStringUtils.randomNumeric(9)));
		}
		Aluno alunoSalvo = dao.saveAndFlush(aluno);

		endereco.setPessoa(alunoSalvo);
		enderecoBC.salvarEndereco(endereco);

		telefone.setPessoa(alunoSalvo);
		telefoneBC.salvarTelefone(telefone);
	}

	public void excluirAluno(Aluno aluno) {
		enderecoBC.excluirEnderecoPorPessoa(aluno);

		telefoneBC.excluirTelefonePorPessoa(aluno);

		dao.delete(aluno);
	}

	public List<Aluno> listarAluno() {
		return dao.findAll();
	}

	public Aluno buscarAluno(Aluno aluno) {
		List<Aluno> listaAluno = dao.getById(aluno.getId());
		return listaAluno.get(0);
	}
}
