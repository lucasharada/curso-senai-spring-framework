package br.com.senai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.com.senai.model.Publicacao;
import br.com.senai.model.repository.IPublicacaoDAO;

@Controller
public class PublicacaoBC {
	@Autowired
	private IPublicacaoDAO dao;
	@Autowired
	private AlunoBC alunoBC;
	
	
	public void salvarPublicacao(Publicacao publicacao) {
		publicacao.setAluno(alunoBC.buscarAluno(publicacao.getAluno()));
		publicacao.setIdProfessor(publicacao.getAluno().getProfessor().getId());
		dao.save(publicacao);
	}

	public void excluirPublicacao(Publicacao publicacao) {
		dao.delete(publicacao);
	}

	public List<Publicacao> listarPublicacao() {
		return dao.findAll();
	}
}
