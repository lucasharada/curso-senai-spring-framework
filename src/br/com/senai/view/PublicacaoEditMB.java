package br.com.senai.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

import br.com.senai.model.Aluno;
import br.com.senai.model.MeioComunicacao;
import br.com.senai.model.Publicacao;
import br.com.senai.support.Fachada;

@Scope(value = WebApplicationContext.SCOPE_REQUEST)
@Named(value = "publicacaoEditMB")
public class PublicacaoEditMB {

	@Autowired
	private Fachada fachada;

	private Publicacao publicacao;
	private List<Aluno> listaAluno;
	private Aluno aluno;
	private List<MeioComunicacao> listaMeioComunicacao;
	private MeioComunicacao meioComunicacao;

	@PostConstruct
	private void init() {
		publicacao = new Publicacao();
		listaAluno = fachada.listarAluno();
		aluno = new Aluno();
		listaMeioComunicacao = fachada.listarMeioComunicacao();
		meioComunicacao = new MeioComunicacao();
	}

	public void preAlterar(Publicacao publicacao) {
		setPublicacao(publicacao);
	}

	public String salvar() {
		publicacao.setAluno(aluno);
		publicacao.setMeioComunicacao(meioComunicacao);
		fachada.salvarPublicacao(publicacao);
		return "success";
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public Publicacao getPublicacao() {
		return publicacao;
	}

	public void setPublicacao(Publicacao publicacao) {
		this.publicacao = publicacao;
	}

	public List<Aluno> getListaAluno() {
		return listaAluno;
	}

	public void setListaAluno(List<Aluno> listaAluno) {
		this.listaAluno = listaAluno;
	}

	public List<MeioComunicacao> getListaMeioComunicacao() {
		return listaMeioComunicacao;
	}

	public void setListaMeioComunicacao(List<MeioComunicacao> listaMeioComunicacao) {
		this.listaMeioComunicacao = listaMeioComunicacao;
	}

	public MeioComunicacao getMeioComunicacao() {
		return meioComunicacao;
	}

	public void setMeioComunicacao(MeioComunicacao meioComunicacao) {
		this.meioComunicacao = meioComunicacao;
	}
}
