package br.com.senai.view;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

import br.com.senai.model.Cidade;
import br.com.senai.model.Endereco;
import br.com.senai.model.Professor;
import br.com.senai.model.Sexo;
import br.com.senai.model.Telefone;
import br.com.senai.model.TipoPessoa;
import br.com.senai.model.Titulacao;
import br.com.senai.support.Fachada;

@Scope(value = WebApplicationContext.SCOPE_REQUEST)
@Named(value = "professorEditMB")
public class ProfessorEditMB {

	@Autowired
	private Fachada fachada;

	private Professor professor;
	private List<Titulacao> listaTitulacao;
	private List<Sexo> enumSexo;
	private Titulacao titulacao;
	private Endereco endereco;
	private List<Cidade> enumCidade;
	private Telefone telefone;

	@PostConstruct
	private void init() {
		professor = new Professor();
		titulacao = new Titulacao();
		endereco = new Endereco();
		endereco.setEstado("Pernambuco");
		telefone = new Telefone();
		listaTitulacao = fachada.listarTitulacao();
		enumSexo = Arrays.asList(Sexo.values());
		enumCidade = Arrays.asList(Cidade.values());
	}

	public void preAlterar(Professor professor) {
		setProfessor(professor);
		setTitulacao(professor.getTitulacao());
		setEndereco(fachada.listarEnderecoPorPessoa(professor).get(0));
		setTelefone(fachada.listarTelefonePorPessoa(professor).get(0));
	}

	public String salvar() {
		professor.setTipoPessoa(TipoPessoa.PROFESSOR);
		professor.setTitulacao(titulacao);
		fachada.salvarProfessor(professor, endereco, telefone);
		return "success";
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public List<Titulacao> getListaTitulacao() {
		return listaTitulacao;
	}

	public void setListaTitulacao(List<Titulacao> listaTitulacao) {
		this.listaTitulacao = listaTitulacao;
	}
	
	public List<Sexo> getEnumSexo() {
		return enumSexo;
	}

	public void setEnumSexo(List<Sexo> enumSexo) {
		this.enumSexo = enumSexo;
	}

	public Titulacao getTitulacao() {
		return titulacao;
	}

	public void setTitulacao(Titulacao titulacao) {
		this.titulacao = titulacao;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Telefone getTelefone() {
		return telefone;
	}

	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}

	public List<Cidade> getEnumCidade() {
		return enumCidade;
	}

	public void setEnumCidade(List<Cidade> enumCidade) {
		this.enumCidade = enumCidade;
	}
}
