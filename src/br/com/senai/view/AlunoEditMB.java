package br.com.senai.view;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

import br.com.senai.model.Aluno;
import br.com.senai.model.Cidade;
import br.com.senai.model.Endereco;
import br.com.senai.model.Professor;
import br.com.senai.model.Sexo;
import br.com.senai.model.Telefone;
import br.com.senai.model.TipoPessoa;
import br.com.senai.support.Fachada;

@Scope(value = WebApplicationContext.SCOPE_REQUEST)
@Named(value = "alunoEditMB")
public class AlunoEditMB {

	@Autowired
	private Fachada fachada;

	private Aluno aluno;
	private List<Sexo> enumSexo;
	private Endereco endereco;
	private List<Cidade> enumCidade;
	private Telefone telefone;
	private List<Professor> listaProfessor;
	private Professor professor;

	@PostConstruct
	private void init() {
		aluno = new Aluno();
		endereco = new Endereco();
		endereco.setEstado("Pernambuco");
		telefone = new Telefone();
		enumSexo = Arrays.asList(Sexo.values());
		enumCidade = Arrays.asList(Cidade.values());
		listaProfessor = fachada.listarProfessor();
		professor = new Professor();
	}

	public void preAlterar(Aluno aluno) {
		setAluno(aluno);
		setEndereco(fachada.listarEnderecoPorPessoa(aluno).get(0));
		setTelefone(fachada.listarTelefonePorPessoa(aluno).get(0));
		setProfessor(aluno.getProfessor());
	}

	public String salvar() {
		aluno.setTipoPessoa(TipoPessoa.ALUNO);
		aluno.setProfessor(professor);
		fachada.salvarAluno(aluno, endereco, telefone);
		return "success";
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public List<Sexo> getEnumSexo() {
		return enumSexo;
	}

	public void setEnumSexo(List<Sexo> enumSexo) {
		this.enumSexo = enumSexo;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Cidade> getEnumCidade() {
		return enumCidade;
	}

	public void setEnumCidade(List<Cidade> enumCidade) {
		this.enumCidade = enumCidade;
	}

	public Telefone getTelefone() {
		return telefone;
	}

	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}

	public List<Professor> getListaProfessor() {
		return listaProfessor;
	}

	public void setListaProfessor(List<Professor> listaProfessor) {
		this.listaProfessor = listaProfessor;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}
}
