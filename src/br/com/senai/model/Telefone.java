package br.com.senai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Telefone extends EntidadeBase<Long> {
	private static final long serialVersionUID = -5963552772369710785L;

	@Column (name = "ddd", length = 3, nullable = false)
	private String ddd;
	@Column (name = "fone", length = 10, nullable = false)
	private String fone;
	@ManyToOne
	private Pessoa pessoa;

	public Telefone() {}
	
	public Telefone(String ddd, String fone, Pessoa pessoa) {
		super();
		this.ddd = ddd;
		this.fone = fone;
		this.pessoa = pessoa;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getFone() {
		return fone;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
}
