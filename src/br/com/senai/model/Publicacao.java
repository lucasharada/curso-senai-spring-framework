package br.com.senai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "publicacao")
public class Publicacao extends EntidadeBase<Long> {
	private static final long serialVersionUID = -3867280333663863073L;

	@Column (name = "titulo", length = 100, nullable = false)
	private String titulo;
	@Column (name = "descricao", length = 500, nullable = false)
	private String descricao;
	@ManyToOne
	private MeioComunicacao meioComunicacao;
	@ManyToOne
	private Aluno aluno;
	@Column (name = "id_professor", nullable = false)
	private Long idProfessor;
	
	public Publicacao() {}

	public Publicacao(String titulo, String descricao, MeioComunicacao meioComunicacao, Aluno aluno, Long idProfessor) {
		super();
		this.titulo = titulo;
		this.descricao = descricao;
		this.meioComunicacao = meioComunicacao;
		this.aluno = aluno;
		this.idProfessor = idProfessor;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public MeioComunicacao getMeioComunicacao() {
		return meioComunicacao;
	}

	public void setMeioComunicacao(MeioComunicacao meioComunicacao) {
		this.meioComunicacao = meioComunicacao;
	}

	public Long getIdProfessor() {
		return idProfessor;
	}

	public void setIdProfessor(Long idProfessor) {
		this.idProfessor = idProfessor;
	}
}
