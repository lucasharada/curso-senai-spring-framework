package br.com.senai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table (name = "professor")
@PrimaryKeyJoinColumn (name = "pessoa_id")
public class Professor extends Pessoa {
	private static final long serialVersionUID = -5000308762884516495L;

	@Column (name = "instituicao_ensino", length = 100, nullable = false)
	private String instituicaoEnsino;
	@Column (name = "data_admissao", length = 30, nullable = false)
	private String dataAdmissao;
	@ManyToOne
	private Titulacao titulacao;
	
	public Professor() {}

	public Professor(String instituicaoEnsino, String dataAdmissao, Titulacao titulacao) {
		super();
		this.instituicaoEnsino = instituicaoEnsino;
		this.dataAdmissao = dataAdmissao;
		this.titulacao = titulacao;
	}

	public String getInstituicaoEnsino() {
		return instituicaoEnsino;
	}

	public void setInstituicaoEnsino(String instituicaoEnsino) {
		this.instituicaoEnsino = instituicaoEnsino;
	}

	public String getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(String dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public Titulacao getTitulacao() {
		return titulacao;
	}

	public void setTitulacao(Titulacao titulacao) {
		this.titulacao = titulacao;
	}
}
