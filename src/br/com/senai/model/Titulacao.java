package br.com.senai.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Titulacao extends EntidadeBase<Long> {
	private static final long serialVersionUID = -4892313521422084155L;

	@Column (name = "titulo", length = 50, nullable = false, unique = true)
	private String titulo;

	public Titulacao() {}
	
	public Titulacao(String titulo) {
		super();
		this.titulo = titulo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
}
