package br.com.senai.model;

public enum Sexo {
	MASCULINO ("Masculino"), 
	FEMININO ("Feminino"), 
	OUTROS ("Outros");
	
	private String sexo;
	
	private Sexo (String sexo){
		this.sexo = sexo;
	}
	
	@Override
	public String toString(){
		return this.sexo;
	}
}
