package br.com.senai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table (name = "pessoa")
@Inheritance (strategy = InheritanceType.JOINED)
public abstract class Pessoa  extends EntidadeBase<Long> {
	private static final long serialVersionUID = -4911555880809782658L;
	
	@Column (name = "cpf", length = 14, unique = true)
	private String cpf;
	@Column (name = "nome", length = 100, nullable = false)
	private String nome;
	@Enumerated(EnumType.ORDINAL)
	@Column (name = "sexo", nullable = false)
	private Sexo sexo;
	@Enumerated(EnumType.ORDINAL)
	@Column (name = "tipo_pessoa", nullable = false)
	private TipoPessoa tipoPessoa;
	
	public Pessoa (){}

	public Pessoa(String cpf, String nome, Sexo sexo, TipoPessoa tipoPessoa) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.sexo = sexo;
		this.tipoPessoa = tipoPessoa;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public TipoPessoa getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(TipoPessoa tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}
}
