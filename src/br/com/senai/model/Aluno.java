package br.com.senai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table (name = "aluno")
@PrimaryKeyJoinColumn (name = "pessoa_id")
public class Aluno extends Pessoa {
	private static final long serialVersionUID = 7746576754553605357L;

	@Column (name = "matricula", nullable = false)
	private int matricula;
	@ManyToOne
	private Professor professor;

	public Aluno() {}

	public Aluno(String cpf, String nome, Sexo sexo, TipoPessoa tipoPessoa, Professor professor) {
		super(cpf, nome, sexo, tipoPessoa);
		this.professor = professor;
	}

	public int getMatricula() {
		return matricula;
	}

	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}
}
