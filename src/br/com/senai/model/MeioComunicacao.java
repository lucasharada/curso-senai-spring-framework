package br.com.senai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table (name = "meio_comunicacao")
public class MeioComunicacao extends EntidadeBase<Long> {
	private static final long serialVersionUID = -6139870890245359976L;
	
	@Column (name = "descricao", length = 100, nullable = false, unique = true)
	private String descricao;

	public MeioComunicacao() {}

	public MeioComunicacao(String descricao) {
		super();
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
