package br.com.senai.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.senai.model.Pessoa;
import br.com.senai.model.Telefone;

public interface ITelefoneDAO extends JpaRepository<Telefone, Long>{

	public List<Telefone> findByPessoa(Pessoa pessoa);
}
