package br.com.senai.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.senai.model.Endereco;
import br.com.senai.model.Pessoa;

public interface IEnderecoDAO extends JpaRepository<Endereco, Long>{

	public List<Endereco> findByPessoa(Pessoa pessoa);
}
