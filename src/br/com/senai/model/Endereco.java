package br.com.senai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Endereco extends EntidadeBase<Long> {
	private static final long serialVersionUID = -5963552772369710785L;

	@Column (name = "logradouro", length = 200, nullable = false)
	private String logradouro;
	@Column (name = "bairro", length = 100, nullable = false)
	private String bairro;
	@Column (name = "numero", nullable = false)
	private String numero;
	@Column (name = "cidade", nullable = false)
	private Cidade cidade;
	@Column (name = "estado", length = 50, nullable = false)
	private String estado;
	@ManyToOne
	private Pessoa pessoa;
	
	public Endereco() {}

	public Endereco(String logradouro, String bairro, String numero, Cidade cidade, String estado, Pessoa pessoa) {
		super();
		this.logradouro = logradouro;
		this.bairro = bairro;
		this.numero = numero;
		this.cidade = cidade;
		this.estado = estado;
		this.pessoa = pessoa;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
}
