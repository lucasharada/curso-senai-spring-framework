package br.com.senai.model;

public enum Cidade {
	RECIFE("Recife"),
	JABOATAO("Jaboat�o dos Guararapes"),
	OLINDA("Olinda"),
	PAULISTA("Paulista"),
	GRAVATA("Gravat�"),
	BEZERROS("Bezerros"),
	CARUARU("Caruaru"),
	GARANHUNS("Garanhuns"),
	PETROLINA("Petrolina"),
	NORONHA("Fernando de Noronha");
	
	private String cidade;
	
	private Cidade(String cidade){
		this.cidade = cidade;
	}
	
	@Override
	public String toString(){
		return this.cidade;
	}
}
