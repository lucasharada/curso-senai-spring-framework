package br.com.senai.support;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import br.com.senai.controller.AlunoBC;
import br.com.senai.controller.EnderecoBC;
import br.com.senai.controller.MeioComunicacaoBC;
import br.com.senai.controller.ProfessorBC;
import br.com.senai.controller.PublicacaoBC;
import br.com.senai.controller.TelefoneBC;
import br.com.senai.controller.TitulacaoBC;
import br.com.senai.controller.UsuarioBC;
import br.com.senai.model.Aluno;
import br.com.senai.model.Endereco;
import br.com.senai.model.MeioComunicacao;
import br.com.senai.model.Pessoa;
import br.com.senai.model.Professor;
import br.com.senai.model.Publicacao;
import br.com.senai.model.Telefone;
import br.com.senai.model.Titulacao;
import br.com.senai.model.Usuario;

@Component
public class Fachada {
	@Autowired
	private UsuarioBC usuarioBC;
	@Autowired
	private MeioComunicacaoBC meioComunicacaoBC;
	@Autowired
	private PublicacaoBC publicacaoBC;
	@Autowired
	private TitulacaoBC titulacaoBC;
	@Autowired
	private ProfessorBC professorBC;
	@Autowired
	private AlunoBC alunoBC;
	@Autowired
	private EnderecoBC enderecoBC;
	@Autowired
	private TelefoneBC telefoneBC;
	
	/* Begin UsuarioBC */
	public void salvarUsuario(Usuario usuario) {
		usuarioBC.salvarUsuario(usuario);
	}

	public void excluirUsuario(Usuario usuario) {
		usuarioBC.excluirUsuario(usuario);
	}

	public List<Usuario> listarUsuario() {
		return usuarioBC.listarUsuario();
	}
	/* End UsuarioBC */

	/* Begin MeioComunicacaoBC */
	public void salvarMeioComunicacao(MeioComunicacao meioComunicacao) throws DataIntegrityViolationException  {
		meioComunicacaoBC.salvarMeioComunicacao(meioComunicacao);
	}

	public void excluirMeioComunicacao(MeioComunicacao meioComunicacao) {
		meioComunicacaoBC.excluirMeioComunicacao(meioComunicacao);
	}
	
	public List<MeioComunicacao> listarMeioComunicacao() {
		return meioComunicacaoBC.listarMeioComunicacao();
	}
	/* End MeioComunicacaoBC */
	
	/* Begin PublicacaoBC */
	public void salvarPublicacao(Publicacao publicacao) {
		publicacaoBC.salvarPublicacao(publicacao);
	}

	public void excluirPublicacao(Publicacao publicacao) {
		publicacaoBC.excluirPublicacao(publicacao);
	}

	public List<Publicacao> listarPublicacao() {
		return publicacaoBC.listarPublicacao();
	}
	/* End PublicacaoBC */
	
	/* Begin TitulacaoBC */
	public void salvarTitulacao(Titulacao titulacao) {
		titulacaoBC.salvarTitulacao(titulacao);
	}

	public void excluirTitulacao(Titulacao titulacao) {
		titulacaoBC.excluirTitulacao(titulacao);
	}
	public List<Titulacao> listarTitulacao() {
		return titulacaoBC.listarTitulacao();
	}
	/* End TitulacaoBC */

	/* Begin ProfessorBC */
	public void salvarProfessor(Professor professor, Endereco endereco, Telefone telefone) {
		professorBC.salvarProfessor(professor, endereco, telefone);
	}

	public void excluirProfessor(Professor professor) {
		professorBC.excluirProfessor(professor);
	}
	
	public List<Professor> listarProfessor() {
		return professorBC.listarProfessor();
	}
	/* End ProfessorBC */
	
	/* Begin AlunoBC */
	public void salvarAluno(Aluno aluno, Endereco endereco, Telefone telefone) {
		alunoBC.salvarAluno(aluno, endereco, telefone);
	}

	public void excluirAluno(Aluno aluno) {
		alunoBC.excluirAluno(aluno);
	}
	
	public List<Aluno> listarAluno() {
		return alunoBC.listarAluno();
	}
	/* End AlunoBC */
	
	/* Begin EnderecoBC */
	public List<Endereco> listarEnderecoPorPessoa(Pessoa pessoa){
		return enderecoBC.listarEnderecoPorPessoa(pessoa);
	}
	/* End EnderecoBC */
	
	/* Begin TelefoneBC */
	public List<Telefone> listarTelefonePorPessoa(Pessoa pessoa){
		return telefoneBC.listarTelefonePorPessoa(pessoa);
	}
	/* End TelefoneBC */
}
